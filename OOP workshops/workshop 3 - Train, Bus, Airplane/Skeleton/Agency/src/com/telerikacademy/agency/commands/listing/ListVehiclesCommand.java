package com.telerikacademy.agency.commands.listing;

import com.telerikacademy.agency.commands.contracts.Command;
import com.telerikacademy.agency.core.contracts.AgencyRepository;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class ListVehiclesCommand implements Command {

    private List<Vehicle> vehicleList;

    private AgencyRepository agencyRepository;

    public ListVehiclesCommand(AgencyRepository agencyRepository) {
        this.agencyRepository = agencyRepository;
    }


    public String execute(List<String> parameters) {
        vehicleList = agencyRepository.getVehicles();

        if (vehicleList.size() == 0) {
            return "There are no registered vehicles.";
        }

        List<String> listVehicle = vehicleToString();

        return String.join(JOIN_DELIMITER + System.lineSeparator(), listVehicle).trim();
    }


    private List<String> vehicleToString() {
        List<String> stringifiedVehicle = new ArrayList<>();
        for (Vehicle vehicle : vehicleList) {
            stringifiedVehicle.add(vehicle.print());
        }
        return stringifiedVehicle;
    }
}

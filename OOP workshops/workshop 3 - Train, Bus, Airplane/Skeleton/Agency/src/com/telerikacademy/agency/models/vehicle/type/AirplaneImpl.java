package com.telerikacademy.agency.models.vehicle.type;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);

        this.hasFreeFood = hasFreeFood;
        setVehicleType(vehicleType);
    }


    public boolean isHasFreeFood() {
        return hasFreeFood;
    }

    public void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }


    @Override
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = VehicleType.Air;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.Air;
    }


    @Override
    public boolean hasFreeFood() {
        return false;
    }

    @Override
    public String print() {
        return String.format("Airplane ----\n" +
                             "Passenger capacity: %d\n" +
                             "Price per kilometer: %.2f\n" +
                             "Vehicle type: %s\n" +
                             "Has free food: %s\n" , passengerCapacity, pricePerKilometer, vehicleType, hasFreeFood);
    }
}

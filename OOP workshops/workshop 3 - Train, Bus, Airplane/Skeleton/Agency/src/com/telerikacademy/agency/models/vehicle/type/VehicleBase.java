package com.telerikacademy.agency.models.vehicle.type;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final int MINIMUM_PASSENGERS_ON_VEHICLE = 1;
    private static final int MAXIMUM_PASSENGERS_ON_VEHICLE = 800;

    int passengerCapacity;
    double pricePerKilometer;
    VehicleType vehicleType;

    public VehicleBase(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MINIMUM_PASSENGERS_ON_VEHICLE || passengerCapacity > MAXIMUM_PASSENGERS_ON_VEHICLE) {
            throw new IllegalArgumentException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < 0.10 || pricePerKilometer > 2.50) {
            throw new IllegalArgumentException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public abstract void setVehicleType(VehicleType vehicleType);
}


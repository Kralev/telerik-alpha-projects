package com.telerikacademy.agency.models.vehicle.type;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int MINIMUM_PASSENGERS_ON_BUS = 10;
    private static final int MAXIMUM_PASSENGERS_ON_BUS = 50;


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);

        setVehicleType(vehicleType);
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MINIMUM_PASSENGERS_ON_BUS || passengerCapacity > MAXIMUM_PASSENGERS_ON_BUS) {
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = VehicleType.Land;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.Land;
    }

    @Override
    public String print() {
        return String.format("Bus ----\n" +
                             "Passenger capacity: %d\n" +
                             "Price per kilometer: %.2f\n" +
                             "Vehicle type: %s\n" , passengerCapacity, pricePerKilometer, vehicleType);
    }
}



package com.telerikacademy.agency.models.vehicle.type;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private static final int MINIMUM_PASSENGERS_ON_TRAIN = 30;
    private static final int MAXIMUM_PASSENGERS_ON_TRAIN = 150;
    private static final int MINIMUM_CARTS_ON_TRAIN = 1;
    private static final int MAXIMUM_CARTS_ON_TRAIN = 15;


    private int carts;


    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);

        this.carts = carts;
        setCarts(carts);
        setVehicleType(vehicleType);
    }

    public void setPassengerCapacity(int passengerCapacity) {

        if (passengerCapacity < MINIMUM_PASSENGERS_ON_TRAIN || passengerCapacity > MAXIMUM_PASSENGERS_ON_TRAIN) {
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
        this.passengerCapacity = passengerCapacity;
    }

    public int getCarts() {
        return carts;
    }

    public void setCarts(int carts) {
        if (carts < MINIMUM_CARTS_ON_TRAIN || carts > MAXIMUM_CARTS_ON_TRAIN) {
            throw new IllegalArgumentException("A train cannot have less than 1 cart or more than 15 carts.");
        }
        this.carts = carts;
    }

    @Override
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = VehicleType.Land;
    }


    @Override
    public VehicleType getType() {
        return VehicleType.Land;
    }


    @Override
    public String print() {
        return String.format("Train ----\n" +
                             "Passenger capacity: %d\n" +
                             "Price per kilometer: %.2f\n" +
                             "Vehicle type: %s\n" +
                             "Carts amount: %d\n" , passengerCapacity, pricePerKilometer, vehicleType, carts);

    }
}
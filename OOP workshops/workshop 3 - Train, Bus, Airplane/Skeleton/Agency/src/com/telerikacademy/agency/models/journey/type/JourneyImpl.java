package com.telerikacademy.agency.models.journey.type;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.vehicle.type.VehicleBase;
import com.telerikacademy.agency.models.vehicles.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class JourneyImpl implements Journey, Printable {


    private static final int START_LOCATION_MINIMUM_NAME_SIZE = 5;
    private static final int START_LOCATION_MAXIMUM_NAME_SIZE = 25;
    private static final int DESTINATION_MINIMUM_NAME_SIZE = 5;
    private static final int DESTINATION_MAXIMUM_NAME_SIZE = 25;
    private static final int JOURNEY_MINIMUM_DISTANCE = 5;
    private static final int JOURNEY_MAXIMUM_DISTANCE = 5000;


    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;
    private Double CalculateTravelCosts;


    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
        if (vehicle == null) {
            throw new IllegalArgumentException("There should be a vehicle");
        }
        setCalculateTravelCosts(calculateTravelCosts());
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        if (startLocation == null) {
            throw new IllegalArgumentException("Start Location cannot be null");
        }
        if (startLocation.length() < START_LOCATION_MINIMUM_NAME_SIZE || startLocation.length() > START_LOCATION_MAXIMUM_NAME_SIZE) {
            throw new IllegalArgumentException("The StartingLocation's length cannot be less than 5 or more than 25 symbols long.");
        }
        this.startLocation = startLocation;
    }


    @Override
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination cannot be null");
        }
        if (destination.length() < DESTINATION_MINIMUM_NAME_SIZE || destination.length() > DESTINATION_MAXIMUM_NAME_SIZE) {
            throw new IllegalArgumentException("The Destination's length cannot be less than 5 or more than 25 symbols long.");
        }
        this.destination = destination;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {

        if (distance < JOURNEY_MINIMUM_DISTANCE || distance > JOURNEY_MAXIMUM_DISTANCE) {
            throw new IllegalArgumentException("The Distance cannot be less than 5 or more than 5000 kilometers.");
        }
        this.distance = distance;

    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }


    @Override
    public double calculateTravelCosts() {

        return distance * vehicle.getPricePerKilometer();
    }

    public void setVehicle(VehicleBase vehicle) {
        if (vehicle == null) {
            throw new IllegalArgumentException("There should be a vehicle");
        }
        this.vehicle = vehicle;
    }

    public Double getCalculateTravelCosts() {
        return CalculateTravelCosts;
    }

    public void setCalculateTravelCosts(Double calculateTravelCosts) {
        CalculateTravelCosts = distance * vehicle.getPricePerKilometer();
    }


    @Override
    public String print() {
        return String.format("Journey ----\n" +
                             "Start location: %s\n" +
                             "Destination: %s\n" +
                             "Distance: %d\n" +
                             "Vehicle type: %s\n" +
                             "Travel costs: %.2f\n" , startLocation, destination, distance, getVehicle().getType(), calculateTravelCosts());
    }
}



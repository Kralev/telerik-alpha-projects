package com.telerikacademy.agency.models.contracts;

import com.telerikacademy.agency.models.vehicles.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public interface Journey extends Printable {

    int a = 5;

    String getDestination();

    int getDistance();

    String getStartLocation();

    Vehicle getVehicle();

    double calculateTravelCosts();
}
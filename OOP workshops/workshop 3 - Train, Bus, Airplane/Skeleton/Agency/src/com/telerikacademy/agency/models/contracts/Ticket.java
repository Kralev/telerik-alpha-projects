package com.telerikacademy.agency.models.contracts;

import com.telerikacademy.agency.models.vehicles.contracts.Printable;

public interface Ticket extends Printable {
    double getAdministrativeCosts();

    Journey getJourney();

    double calculatePrice();
}

package com.telerikacademy.agency.models.vehicles.contracts;

public interface Train extends Vehicle, Printable {
    int getCarts();
}

package com.telerikacademy.agency.models.common;

public enum VehicleType {
    Land,
    Air,
    Sea;
}

package com.telerikacademy.agency.models.journey.type;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;
import com.telerikacademy.agency.models.vehicles.contracts.Printable;

public class TicketImpl implements Ticket, Printable {

    private Journey journey;
    private double AdministrativeCosts;


    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        AdministrativeCosts = administrativeCosts;
    }


    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public void setAdministrativeCosts(double administrativeCosts) {
        AdministrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return 0;
    }

    @Override
    public Journey getJourney() {
        return null;
    }

    @Override
    public double calculatePrice() {

        return AdministrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public String print() {
        return String.format("Ticket ----\n" +
                             "Destination: %s\n" +
                             "Price: %.2f\n" , journey.getDestination(), calculatePrice());
    }
}

package com.telerikacademy.agency.models.vehicles.contracts;

public interface Printable {

    String print();
}

package com.telerikacademy.agency.models.vehicles.contracts;

public interface Airplane extends Vehicle, Printable {
    boolean hasFreeFood();
}

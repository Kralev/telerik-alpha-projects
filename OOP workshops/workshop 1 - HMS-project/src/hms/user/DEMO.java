package hms.user;

import hms.user.billing.Billing;
import hms.user.insurancePlans.*;
import hms.user.patient.Patient;

public class DEMO {
    public static void main(String[] args) {

        Patient Pesho = new Patient(32323, "Pesho", "Kralev", "Male", "sdsd@mail.bg", true, 23, true);

        Pesho.setInsurancePlan(new GoldPlan());

        Billing billing = new Billing();

        double finalResult[] = billing.computePaymentAmount(Pesho, 1000.0);
        System.out.print("Agency will pay: ");
        System.out.println(finalResult[0]);
        System.out.print("Pesho will pay: ");
        System.out.println(finalResult[1]);
        System.out.println();
        System.out.println("///////////Test 1 //////////");
        System.out.println();

        ////////////////////////////////////////////////////


        //////////////////////////////////////////////////////


        HealthInsurancePlan healthInsurancePlan = new PlatinumPlan();

        User staff = new User(232,"Simon", "Kralev", "Male", "sds@yahoo.com", true, 65, true);
        InsuranceBrand insuranceBrand = new BlueCrossBlueShield();
        HealthInsurancePlan insurancePlan = new PlatinumPlan();

        insurancePlan.setOfferedBy(insuranceBrand);
        staff.setInsurancePlan(insurancePlan);
        System.out.printf("Monthly premium is: %.2f",insurancePlan.computeMonthlyPremium(5000,56,true));

    }
}
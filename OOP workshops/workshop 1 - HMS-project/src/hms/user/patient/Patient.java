package hms.user.patient;

import hms.user.User;

public class Patient extends User {

    public Patient(long id, String fistName, String lastName, String gender, String email, boolean isInsured, int age, boolean isSmoker) {
        super(id, fistName, lastName, gender, email, isInsured, age, isSmoker);
    }
}



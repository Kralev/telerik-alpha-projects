package hms.user.staff;

import hms.user.User;

public class Staff extends User {

    private int yearsOfExperience;
    private String description;
    private double salary;

    public Staff(long id,String fistName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary, boolean isInsured, int age, boolean isSmoker){
        super(id,fistName,lastName,gender,email,isInsured, age, isSmoker);

        this.yearsOfExperience = yearsOfExperience;
        this.description = description;
        this.salary = salary;
    }

    private int getYearsOfExperience() {
        return yearsOfExperience;
    }

    private String getDescription() {
        return description;
    }

    private double getSalary() {
        return salary;
    }

    private void setYearsOfExperience(int yearsOfExperience) {

        this.yearsOfExperience = yearsOfExperience;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setSalary(double salary) {
        this.salary = salary;
    }
}

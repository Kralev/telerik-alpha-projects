package hms.user.staff;

public class Doctor  extends Staff {

    private String specialization;

    public Doctor(long id, String fistName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary, String specialization, boolean isInsured, int age, boolean isSmoker) {
        super(id, fistName, lastName, gender, email, yearsOfExperience, description, salary, isInsured, age, isSmoker);
        this.specialization = specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getSpecialization() {

        return specialization;
    }
}

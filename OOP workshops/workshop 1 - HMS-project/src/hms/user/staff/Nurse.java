package hms.user.staff;

public class Nurse extends Staff {
    public Nurse(long id, String fistName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary, boolean isInsured, int age, boolean isSmoker) {
        super(id, fistName, lastName, gender, email, yearsOfExperience, description, salary, isInsured, age, isSmoker);
    }
}
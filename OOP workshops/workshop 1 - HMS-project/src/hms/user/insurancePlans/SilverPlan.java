package hms.user.insurancePlans;

public class SilverPlan extends HealthInsurancePlan {

    private static final double SILVER_COVERAGE_VALUE = 0.7;
    private static final int SILVER_DISCOUNT_VALUE = 30;
    private static final int AGE_PREMIUM = 100;
    private static final int SMOKE_PREMIUM = 80;

    @Override
    public double getCoverage() {
        return SILVER_COVERAGE_VALUE;
    }

    @Override
    public double getDiscount() {
        return SILVER_DISCOUNT_VALUE;
    }

    @Override
    public int agePremium() {
        return AGE_PREMIUM;
    }

    @Override
    public int smokePremium() {
        return SMOKE_PREMIUM;
    }


    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        return (salary * 6) / 100 + getofferedBy().computeMonthlyPremium(new SilverPlan(), age, smoking);
    }
}

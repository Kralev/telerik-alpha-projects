package hms.user.insurancePlans;

public abstract class HealthInsurancePlan {

    private double coverage;
    private int discount;
    private InsuranceBrand offeredBy;

    public InsuranceBrand getofferedBy() {
        return offeredBy;
    }

    public void setOfferedBy(InsuranceBrand offeredBy) {
        this.offeredBy = offeredBy;
    }

    private void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    public abstract double getCoverage();

    public abstract int agePremium();

    public abstract int smokePremium();

    public abstract double getDiscount();

    public abstract double computeMonthlyPremium(double salary, int age, boolean smoking);
}
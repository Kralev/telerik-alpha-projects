package hms.user.insurancePlans;

public class PlatinumPlan extends HealthInsurancePlan{

    private static final double PLATINUM_COVERED_VALUE = 0.9;
    private static final int PLATINUM_DISCOUNT_VALUE = 50;
    private static final int AGE_PREMIUM = 200;
    private static final int SMOKE_PREMIUM = 100;

    @Override
    public double getCoverage() {
        return PLATINUM_COVERED_VALUE;
    }

    @Override
    public double getDiscount() {
        return PLATINUM_DISCOUNT_VALUE;
    }

    @Override
    public int agePremium() {
        return AGE_PREMIUM;
    }

    @Override
    public int smokePremium() {
        return SMOKE_PREMIUM;
    }


    @Override
    public double computeMonthlyPremium(double salary, int age , boolean smoking) {
        return ((salary * 8) / 100) + getofferedBy().computeMonthlyPremium(new PlatinumPlan(), age, smoking);
    }
}

package hms.user.insurancePlans;

public class BronzePlan extends HealthInsurancePlan{

    private static final double PLATINUM_COVERED_VALUE = 0.6;
    private static final int DISCOUNT = 25;
    private static final int AGE_PREMIUM = 50;
    private static final int SMOKE_PREMIUM = 70;

    @Override
    public double getCoverage() {
        return PLATINUM_COVERED_VALUE;
    }

    @Override
    public double getDiscount() {
        return DISCOUNT;
    }

    @Override
    public int agePremium() {
        return AGE_PREMIUM;
    }

    @Override
    public int smokePremium() {
        return SMOKE_PREMIUM;
    }

    @Override
    public double computeMonthlyPremium(double salary, int age , boolean smoking) {
        return (salary * 5) / 100 + getofferedBy().computeMonthlyPremium(new BronzePlan(), age, smoking);
    }
}

package hms.user.insurancePlans;

public class BlueCrossBlueShield implements InsuranceBrand{

    private long id;
    private String name;

    public BlueCrossBlueShield() { }

    public BlueCrossBlueShield(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking) {
        final int ageForPremium = 55;
        double premiumSum = 0;

        if (age > ageForPremium) {
            premiumSum += insurancePlan.agePremium();
        }
        if (smoking) {
            premiumSum += insurancePlan.smokePremium();
        }

        return premiumSum;
    }
}

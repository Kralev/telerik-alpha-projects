package hms.user.insurancePlans;

public class GoldPlan extends HealthInsurancePlan {

    private static final double PLATINUM_COVERED_VALUE = 0.8;
    private static final int DISCOUNT = 40;
    private static final int AGE_PREMIUM = 150;
    private static final int SMOKE_PREMIUM = 90;

    @Override
    public double getCoverage() {
        return PLATINUM_COVERED_VALUE;
    }

    @Override
    public double getDiscount() {
        return DISCOUNT;
    }

    @Override
    public int agePremium() {
        return AGE_PREMIUM;
    }

    @Override
    public int smokePremium() {
        return SMOKE_PREMIUM;
    }

    @Override
    public double computeMonthlyPremium(double salary, int age, boolean smoking) {
        return (salary * 7) / 100 + getofferedBy().computeMonthlyPremium(new GoldPlan(), age, smoking);
    }
}


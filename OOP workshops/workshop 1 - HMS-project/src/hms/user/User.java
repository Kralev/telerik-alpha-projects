package hms.user;

import hms.user.insurancePlans.HealthInsurancePlan;

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private boolean isInsured;
    private int age;
    private boolean isSmoker;



    public User(long id, String firstName, String lastName, String gender, String email, boolean isInsured, int age, boolean isSmoker) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.isInsured = isInsured;
        this.age = age;
        this.isSmoker = isSmoker;


    }

    private long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    private String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String getGender() {
        return gender;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }

    private String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public void setInsured(boolean insured) {
        this.isInsured = insured;
    }

    public boolean isInsured() {

        return isInsured;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSmoker() {
        return isSmoker;
    }

    public void setSmoker(boolean smoker) {
        isSmoker = smoker;
    }

    private HealthInsurancePlan InsurancePlan = null;


    public HealthInsurancePlan getInsurancePlan() {
        return InsurancePlan;
    }

    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        InsurancePlan = insurancePlan;
    }
}

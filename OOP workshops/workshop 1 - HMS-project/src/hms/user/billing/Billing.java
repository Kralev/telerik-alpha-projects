package hms.user.billing;

import hms.user.patient.Patient;

public class Billing {

    double[] result = new double[2];

    public double[] computePaymentAmount(Patient patient, Double amount) {

        if (patient.isInsured()) {

            result[0] = amount * patient.getInsurancePlan().getCoverage();
            result[1] = amount - result[0] - patient.getInsurancePlan().getDiscount();
            return result;
        } else {

            result[0] = 0;
            result[1] = amount - 20;
            return result;

        }
    }
}




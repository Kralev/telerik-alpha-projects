package com.telerikacademy.cosmetics.models.common;

public enum UsageType {
    EVERY_DAY {
        public String toString() {
            return "EVERY_DAY";
        }
    },
    EVERYDAY,
    MEDICAL
}

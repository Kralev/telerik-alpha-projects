package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.List;

public class ToothpasteImpl extends BathroomProducts implements Product, Toothpaste {

    private static final int TOOTHPASTE_MINIMUM_NAME_SIZE = 3;
    private static final int TOOTHPASTE_MAXIMUM_NAME_SIZE = 10;
    private static final int TOOTHPASTE_MINIMUM_BRAND_SIZE = 2;
    private static final int TOOTHPASTE_MAXIMUM_BRAND_SIZE = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name,brand,price,gender);
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
        setIngredients(ingredients);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if (name.length() < TOOTHPASTE_MINIMUM_NAME_SIZE || name.length() > TOOTHPASTE_MAXIMUM_NAME_SIZE) {
            throw new IllegalArgumentException("Name should be between 3 and 10 symbols");
        }
        this.name = name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if (brand.length() < TOOTHPASTE_MINIMUM_BRAND_SIZE || brand.length() > TOOTHPASTE_MAXIMUM_BRAND_SIZE) {
            throw new IllegalArgumentException("Brand should be between 2 and 10 symbols");
        }
        this.brand = brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price can not be 0");
        }
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    @Override
    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients cannot be null");
        }
        this.ingredients = ingredients;
    }

    @Override
    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n" +
                " #Ingredients: %s\n" +
                " ===", name, brand, price, gender, ingredients);
    }
}
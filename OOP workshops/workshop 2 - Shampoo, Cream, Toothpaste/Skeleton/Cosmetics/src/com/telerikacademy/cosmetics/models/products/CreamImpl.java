package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class CreamImpl extends BathroomProducts implements Product, Cream {

    private static final int CREAM_MINIMUM_NAME_SIZE = 3;
    private static final int CREAM_MAXIMUM_NAME_SIZE = 15;
    private static final int CREAM_MINIMUM_BRAND_SIZE = 3;
    private static final int CREAM_MAXIMUM_BRAND_SIZE = 15;


    private String name;
    private String brand;
    private double price;
    private GenderType gender;
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name,brand,price,gender);
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
        setScent(scent);
    }


    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if (name.length() < CREAM_MINIMUM_NAME_SIZE || name.length() > CREAM_MAXIMUM_NAME_SIZE) {
            throw new IllegalArgumentException("Name should be between 3 and 15 symbols");
        }
        this.name = name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("Brand cannot be null");
        }
        if (brand.length() < CREAM_MINIMUM_BRAND_SIZE || brand.length() > CREAM_MAXIMUM_BRAND_SIZE) {
            throw new IllegalArgumentException("Brand should be between 3 and 15 symbols");
        }
        this.brand = brand;
    }


    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price can not be 0");
        }
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public ScentType getScent() {
        return scent;
    }

    public void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n" +
                " #Usage: %s\n" +
                " #Scent: %s\n" +
                " ===", name, brand, price, gender, scent);
    }
}

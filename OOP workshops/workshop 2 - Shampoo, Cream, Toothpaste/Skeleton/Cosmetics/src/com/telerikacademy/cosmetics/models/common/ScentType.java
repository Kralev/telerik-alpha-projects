package com.telerikacademy.cosmetics.models.common;

public enum ScentType {
    lavender,
    vanilla,
    rose
}

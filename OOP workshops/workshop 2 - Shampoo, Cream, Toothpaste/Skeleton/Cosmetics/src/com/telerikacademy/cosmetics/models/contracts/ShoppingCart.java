package com.telerikacademy.cosmetics.models.contracts;

import java.util.List;

public interface ShoppingCart {
    void addProduct(Product product);

    boolean containsProduct(Product product);

    void removeProduct(Product product);

    double totalPrice();

    List<Product> getProductList();
}

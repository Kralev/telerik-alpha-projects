package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends BathroomProducts implements Product, Shampoo {

    private static final int SHAMPOO_MINIMUM_NAME_SIZE = 3;
    private static final int SHAMPOO_MAXIMUM_NAME_SIZE = 10;
    private static final int SHAMPOO_MINIMUM_BRAND_SIZE = 2;
    private static final int SHAMPOO_MAXIMUM_BRAND_SIZE = 10;


    private String name;
    private String brand;
    private double price;
    private GenderType gender;
    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name,brand,price,gender);
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
        setMilliliters(milliliters);
        this.usage = usage;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if (name.length() < SHAMPOO_MINIMUM_NAME_SIZE || name.length() > SHAMPOO_MAXIMUM_NAME_SIZE) {
            throw new IllegalArgumentException("Name should be between 3 and 10 symbols");
        }
        this.name = name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("Brand cannot be null");
        }
        if (brand.length() < SHAMPOO_MINIMUM_BRAND_SIZE || brand.length() > SHAMPOO_MAXIMUM_BRAND_SIZE) {
            throw new IllegalArgumentException("Brand should be between 2 and 10 symbols");
        }
        this.brand = brand;
    }


    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price can not be 0");
        }
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    private int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters can not be 0");
        }
        this.milliliters = milliliters;
    }

    private UsageType getUsage() {
        return usage;
    }

    private void setUsage(UsageType usage) {
        this.usage = usage;
    }

    @Override
    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n" +
                " #Usage: %s\n" +
                " ===", name, brand, price, gender, usage);
    }
}
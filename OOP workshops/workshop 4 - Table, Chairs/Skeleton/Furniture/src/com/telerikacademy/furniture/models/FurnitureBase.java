package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class FurnitureBase implements Furniture {

    private static final int MODEL_MIN_NAME = 3;

    public String model;
    public MaterialType materialType;
    public double price;
    public double height;

    public FurnitureBase(String model, MaterialType materialType, double price, double height) {
        setModel(model);
        this.materialType = materialType;
        setPrice(price);
        setHeight(height);
    }


    private void setModel(String model) {
        if (model == null || model.length() < MODEL_MIN_NAME) {
            throw new IllegalArgumentException("Model cannot be empty, null or with less than 3 symbols.");
        }
        this.model = model;
    }

    @Override
    public String getModel() {
        return model;
    }


    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }


    private void setPrice(double price) {
        if (price <= 0) {
            throw new IllegalArgumentException("Price cannot be less or equal to 0.00.");
        }
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setHeight(double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height cannot be less or equal to 0.00.");
        }
        this.height = height;
    }

    @Override
    public double getHeight() {
        return height;
    }
}

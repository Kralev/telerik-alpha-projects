package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Table;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureBase implements Table {

    private double length;
    private double width;

    public TableImpl(String model, MaterialType materialType, double price, double height, double length, double width) {
        super(model, materialType, price, height);
        setLength(length);
        setWidth(width);
    }


    private void setLength(double length) {
        if (length <= 0) {
            throw new IllegalArgumentException("Length should be greater that 0");
        }
        this.length = length;
    }

    @Override
    public double getLength() {
        return length;
    }

    private void setWidth(double width) {
        if (width <= 0) {
            throw new IllegalArgumentException("Width should be greater that 0");
        }
        this.width = width;
    }


    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Length: %.2f, Width: %.2f, Area: %.4f",
                this.getClass().getSimpleName().replace("Impl", ""), model, materialType, price, height, length, width, getArea());
    }
}

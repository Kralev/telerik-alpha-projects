package com.telerikacademy.furniture.models.contracts;

import com.telerikacademy.furniture.models.enums.MaterialType;

public interface Table extends Furniture{
    double getLength();

    double getWidth();

    double getArea();

    MaterialType getMaterialType();
}

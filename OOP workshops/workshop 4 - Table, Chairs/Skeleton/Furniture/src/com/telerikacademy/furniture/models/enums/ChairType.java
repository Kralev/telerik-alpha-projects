package com.telerikacademy.furniture.models.enums;

public enum  ChairType {
    normal,
    adjustable,
    convertible
}

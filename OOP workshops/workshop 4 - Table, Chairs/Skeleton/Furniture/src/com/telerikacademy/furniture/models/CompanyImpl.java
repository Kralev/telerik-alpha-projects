package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CompanyImpl implements Company {
    private static final int NAME_MIN_LENGTH = 5;
    private static final double EXACT_REGISTRATION_NUMBER_LENGTH = 10;

    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber ) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<>();
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    private void setRegistrationNumber(String registrationNumber) {
        if (registrationNumber.length() != EXACT_REGISTRATION_NUMBER_LENGTH) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
        this.registrationNumber = registrationNumber;
    }


    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.isEmpty() || name.length() < NAME_MIN_LENGTH) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }


    public List<Furniture> getFurnitures() {
        return new ArrayList<>(furnitures);
    }

    public void add(Furniture furniture) {
        if (furniture == null){
            throw new IllegalArgumentException("There should be a furniture.");
        }
        furnitures.add(furniture);
    }

    public String catalog() {
        furnitures.sort((x1, x2) -> {
            int compareResult = (int) (x1.getPrice() - x2.getPrice());
            if (compareResult == 0) {
                compareResult = x1.getModel().compareTo(x2.getModel());
            }
            return compareResult;
        });

        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(toString());
        strBuilder.append("\n");
        for (Furniture furniture : furnitures) {
            strBuilder.append(furniture.toString());
            strBuilder.append("\n");
        }
        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        if (model == null){
            throw new IllegalArgumentException();
        }

        Furniture item = furnitures.stream()
                .filter(x -> x.getModel().equalsIgnoreCase(model))
                .findFirst()
                .orElse(null);

        return item;
    }

    public void remove(Furniture furniture) {
        if (furniture == null) {
            throw new IllegalArgumentException();
        }
        furnitures.remove(furniture);
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %s %s",
                name,
                registrationNumber,
                furnitures.isEmpty() ? "no" : String.format("%d", furnitures.size()),
                furnitures.size() == 1 ? "furniture" : "furnitures");
    }
}
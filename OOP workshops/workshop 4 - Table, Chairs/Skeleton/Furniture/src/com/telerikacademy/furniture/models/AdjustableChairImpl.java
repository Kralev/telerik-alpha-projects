package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends FurnitureBase implements AdjustableChair {

    private int numberOfLegs;
    String type;


    public AdjustableChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);

        setNumberOfLegs(numberOfLegs);
        setType(type);


    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getHeight() {
        return super.getHeight();
    }

    public void setHeight(double height) {
        if (height <= 0) {
            throw new IllegalArgumentException("Height should be positive.");
        }
        super.setHeight(height);
    }


    private void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Legs: %d",
                this.getClass().getSimpleName().replace("Impl", ""), model, materialType, price, height, numberOfLegs);
    }
}

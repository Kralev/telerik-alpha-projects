package com.telerikacademy.furniture.commands.enums;

public enum ConvertibleChairState {
    NORMAL,
    CONVERTED;
}

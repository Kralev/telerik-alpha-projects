package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.commands.enums.ConvertibleChairState;
import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.enums.MaterialType;

import static com.telerikacademy.furniture.commands.enums.ConvertibleChairState.CONVERTED;
import static com.telerikacademy.furniture.commands.enums.ConvertibleChairState.NORMAL;

public class ConvertibleChairImpl extends FurnitureBase implements ConvertibleChair {

    private static final double CONVERTED_HEIGHT = 0.1;

    private double height;
    private int numberOfLegs;
    private ConvertibleChairState state;
    String type;


    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);

        this.height = super.getHeight();
        this.state = NORMAL;
        setNumberOfLegs(numberOfLegs);
        setType(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public boolean getConverted() {
        if (state.equals(CONVERTED)) {
            return true;
        }
        return false;
    }

    @Override
    public void convert() {
        if (state.equals(NORMAL)) {
            this.state = CONVERTED;
            this.height = CONVERTED_HEIGHT;
        } else {
            this.state = NORMAL;
            this.height = super.getHeight();
        }
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Legs: %d, State: %s",
                this.getClass().getSimpleName().replace("Impl", ""), model, materialType, price, height, numberOfLegs, getConverted() ? CONVERTED : NORMAL);

    }
}



package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;

import com.telerikacademy.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureBase implements Chair {

    private int numberOfLegs;

    String type;

    public ChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);

        setNumberOfLegs(numberOfLegs);
        setType(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        if (numberOfLegs < 0) {
            throw new IllegalArgumentException("Legs should be more than 0");
        }
        this.numberOfLegs = numberOfLegs;
    }


    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Legs: %d",
                this.getClass().getSimpleName().replace("Impl", ""), model, materialType, price, height, numberOfLegs);
    }
}
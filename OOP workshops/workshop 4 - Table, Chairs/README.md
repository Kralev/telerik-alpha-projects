## OOP Workshop - Principles (Furniture)



- The listed furniture added to a certain company (through the add(…) method) must be **ordered by price then by model**. 
  - If the company has no furniture added, print “no furnitures” (**yes, “furnitures” is not a valid word, these are the client's requirements**).
  - If the company has 1 piece of furniture, print “1 furniture” and show its information on a separate line. - If the company has more than 1 piece of furniture, print its number and list each one’s information on a separate line. 
  - Choose:
    - All double type fields should be printed “as is”, without any formatting or rounding. 
    - Or use the following formatting: 0.00 for all double values except getArea(). The area should be print in 0.0000 format (This is the printed example output.)


### Printing


- The Type is either “Table“, or “Chair”, or “AdjustableChair” or “ConvertibleChair”. The convertible chair state is either
 “Converted” or “Normal”. All double type fields should be printed “as is”, without any formatting or rounding.

- If a null value is passed to some mandatory property, you should use **defensive programming** to prevent unwanted results.

### Commands
Implement the CreateTable command in the commands folder. Study the already implemented commands to understand how to do it.

The command should follow the following specification:

**CreateTable (model) (material) (price) (height) (length) (width)** – creates a table with given model, material, price, height, length and width. Duplicate models are not allowed. As a result the command returns “Table (model) created”.

### Additional Notes
To simplify your work there is already working engine that executes a sequence of commands read from the console using the classes and interfaces in your project. Please put your classes in package **com.telerikacademy.furnituremanufacturer.models;**. Implement the **FurnitureFactoryImpl** class in the package **com.telerikacademy.furnituremanufacturer.engine.factories;**.


Current implemented commands the engine supports are:
- **CreateCompany (name) (registration number)** – adds a company with given name and registration number. Duplicate names are not allowed. As a result the command returns “Company (name) created”.
- **AddFurnitureToCompany (company name) (furniture model)** – searches for furniture and adds it to an existing company’s catalog. As a result the command returns “Furniture (furniture model) added to company (company name)”.
- **RemoveFurnitureFromCompany (company name) (furniture model)** – searches for furniture and removes it from an existing company’s catalog. As a result the command returns “Furniture (furniture model) removed from company (company name)”.
- **FindFurnitureFromCompany (company name) (furniture model)** – searches for furniture in an existing company’s catalog. If found the engine prints the furniture’s toString() method.
- **ShowCompanyCatalog (company name)** – searches for a company and invokes it’s catalog() method.

- **CreateChair (model) (material) (price) (height) (legs) (type)** – creates a chair by given model, material, price, height, legs and type. Type can be “Normal”, “Adjustable” and “Convertible”. Duplicate models are not allowed. As a result the command returns “Chair (model) created”.
- **SetChairHeight (model) (height)** – searches for a chair by model and sets its height, if the chair is adjustable. As a result the command returns “Chair (model) adjusted to height (height)”.
- **ConvertChair (model)** – searches for a chair by model and converts its state, if the chair is convertible. As a result the command returns “Chair (model) converted”.
In case of invalid operation or error, the engine returns appropriate text messages.



**All commands return appropriate success messages. In case of invalid operation or error, the engine returns appropriate error messages.**


## Step by step guide

**HINT** - **DO NOT** use the whole input as a test. Break it as simple as possible. Maybe one line at a time is very good starting point.

**-1.** The classes are there. Use them with caution.

**0.** Implement all the interfaces. 

- Look at the "contracts" folder and decide how to use any of the interfaces there.

**1.** General Hints

- You need to override toString() in order to output the classes in the console.

**2.** You are given a skeleton of the Furniture Factory. Please take a look at it carefully before you try to do anything. Try to understand all the classes and interfaces and how the engine works. (You do not need to touch the engine at all).

**3.** Just build the project and look at the errors.
  - **HINT** implement the classes 
  - You already know how to do it. Use inheritance.

**4.** Are there any inheritance options? Do you have repeating code in more than one class?
  - Maybe one or mode base classes?

**5.** Take a look at the FurnitureFactoryImpl class
  - Is there an interface we could implement?

**6.** How we can validate the that the string passed is a valid enum defined in our solution
  - Look inside FurnitureFactoryImpl class and use the method below

```java
 private MaterialType getMaterialType (String material){
            return MaterialType.valueOf(material.toUpperCase());
        }
```




**7.** Validate all properties according to the guidelines set above

**HINT** on how to validate the company's registration number

```java
        if (registrationNumber.length() != EXACT_REGISTRATION_NUMBER_LENGTH) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
```

**8.** Implement the methods inside the CompanyImpl class
  - Here is how to get first or default item from collection and how to compare two items and sort collection

```java
Furniture item = furnitures.stream()
                .filter(x -> x.getModel().equalsIgnoreCase(model))
                .findFirst()
                .orElse(null);

furnitures.sort((x1, x2) -> {
            int compareResult = (int) (x1.getPrice() - x2.getPrice());
            if (compareResult == 0) {
                compareResult = x1.getModel().compareTo(x2.getModel());
            }
            return compareResult;
        });
```

**9.** Implement the CreateTable command

### Sample Input

```
CreateCompany FurnitureTelerik 1234567890
ShowCompanyCatalog FurnitureTelerik
CreateTable SmallTable wooden 123.4 0.50 0.45 0.65
CreateChair TestChair leather 99.99 1.20 5 Normal
CreateChair MyChair leather 111.56 0.80 4 Adjustable
CreateChair NewChair plastic 80.00 1.00 3 Convertible
ShowCompanyCatalog FurnitureTelerik
AddFurnitureToCompany FurnitureTelerik SmallTable
AddFurnitureToCompany FurnitureTelerik TestChair
AddFurnitureToCompany FurnitureTelerik MyChair
AddFurnitureToCompany FurnitureTelerik NewChair
ShowCompanyCatalog FurnitureTelerik
RemoveFurnitureFromCompany FurnitureTelerik NewChair
ShowCompanyCatalog FurnitureTelerik
FindFurnitureFromCompany FurnitureTelerik MyChair
FindFurnitureFromCompany FurnitureTelerik NewChair
RemoveFurnitureFromCompany FurnitureTelerik MyChair
RemoveFurnitureFromCompany FurnitureTelerik SmallTable
ShowCompanyCatalog FurnitureTelerik
Exit
```

### Sample Output with 0.00 formatting (0.0000 for the area)

```
Company FurnitureTelerik created
FurnitureTelerik - 1234567890 - no furnitures
Table SmallTable created
Chair TestChair created
Chair MyChair created
Chair NewChair created
FurnitureTelerik - 1234567890 - no furnitures
Furniture SmallTable added to company FurnitureTelerik
Furniture TestChair added to company FurnitureTelerik
Furniture MyChair added to company FurnitureTelerik
Furniture NewChair added to company FurnitureTelerik
FurnitureTelerik - 1234567890 - 4 furnitures
Type: ConvertibleChair, Model: NewChair, Material: Plastic, Price: 80.00, Height: 1.00, Legs: 3, State: Normal
Type: Chair, Model: TestChair, Material: Leather, Price: 99.99, Height: 1.20, Legs: 5
Type: AdjustableChair, Model: MyChair, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
Type: Table, Model: SmallTable, Material: Wooden, Price: 123.40, Height: 0.50, Length: 0.45, Width: 0.65, Area: 0.2925
Furniture NewChair removed from company FurnitureTelerik
FurnitureTelerik - 1234567890 - 3 furnitures
Type: Chair, Model: TestChair, Material: Leather, Price: 99.99, Height: 1.20, Legs: 5
Type: AdjustableChair, Model: MyChair, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
Type: Table, Model: SmallTable, Material: Wooden, Price: 123.40, Height: 0.50, Length: 0.45, Width: 0.65, Area: 0.2925
Type: AdjustableChair, Model: MyChair, Material: Leather, Price: 111.56, Height: 0.80, Legs: 4
Furniture NewChair not found
Furniture MyChair removed from company FurnitureTelerik
Furniture SmallTable removed from company FurnitureTelerik
FurnitureTelerik - 1234567890 - 1 furniture
Type: Chair, Model: TestChair, Material: Leather, Price: 99.99, Height: 1.20, Legs: 5
```

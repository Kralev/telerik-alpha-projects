package com.company;

import java.util.Iterator;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {


    private DoubleNode<T> head;
    private DoubleNode<T> tail;
    private int count;

    public DoublyLinkedListImpl() {
        head = null;
        tail = null;
        count = 0;
    }

    @Override
    public DoubleNode getHead() {
        if (count == 0) {
            throw new IllegalArgumentException();
        } else {
            return head;
        }
    }

    @Override
    public DoubleNode getTail() {
        if (count == 0) {
            throw new IllegalArgumentException();
        } else {
            return tail;
        }
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void addLast(T value) {
        if (tail == null) {
            tail = new DoubleNode<>(value, null, null);
            head = tail;
        } else {
            DoubleNode save = new DoubleNode<>(value, tail, null);
            tail.setNext(save);
            tail = save;
        }
        count++;
    }

    @Override
    public void addFirst(T value) {

        if (head == null) {
            head = new DoubleNode<>(value, null, null);
            tail = head;
        } else {
            DoubleNode<T> save = new DoubleNode<>(value, null, head);
            head.setPrev(save);
            head = save;
        }
        count++;
    }

    @Override
    public void insertBefore(DoubleNode node, T value) {

        if (node == null)
            throw new NullPointerException();

        DoubleNode<T> save = head;

        while (save != null) {

            if (save == node) {
                DoubleNode<T> saveNode;

                if (save.getPrev() == null) {

                    saveNode = new DoubleNode<>(value, null, save);
                    save.setPrev(saveNode);
                    head = saveNode;
                } else {
                    saveNode = new DoubleNode<>(value, save.getPrev(), save);
                    save.getPrev().setNext(saveNode);
                    save.setPrev(saveNode);
                }
                count++;
                return;
            }
            save = save.getNext();
        }
    }

    @Override
    public void insertAfter(DoubleNode node, T value) {
        if (node == null)
            throw new NullPointerException();

        DoubleNode<T> save = head;

        while (save != null) {
            DoubleNode<T> saveNode;

            if (save == node) {
                if (save.getNext() == null) {
                    saveNode = new DoubleNode<>(value, save, null);
                    save.setNext(saveNode);
                    tail = saveNode;
                } else {
                    saveNode = new DoubleNode<>(value, save, save.getNext());
                    save.getNext().setPrev(saveNode);
                    save.setNext(saveNode);
                }
                count++;
                return;
            }
            save = save.getNext();
        }
    }

    @Override
    public T removeFirst() {
        if (count == 0)
            throw new NullPointerException();

        T save = head.getValue();

        if (count == 1) {
            tail = head = null;
            return save;
        }

        head = head.getNext();
        head.setPrev(null);
        count--;
        return save;
    }

    @Override
    public T removeLast() {
        if (count == 0)
            throw new NullPointerException();
        T save = tail.getValue();
        if (count == 1) {
            tail = head = null;
            return save;
        }

        tail = tail.getPrev();
        tail.setNext(null);
        count--;
        return save;
    }

    @Override
    public DoubleNode find(T value) {
        if (count == 0)
            return null;

        DoubleNode save = head;
        while (save != null) {
            if (save.getValue() == value) {
                return save;
            }
            save = save.getNext();
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {

        return new DoubleListIterator();
    }

    private class DoubleListIterator implements Iterator<T> {
        private DoubleNode<T> node;

        public DoubleListIterator() {
            node = head;
        }

        @Override
        public boolean hasNext() {
            return node != null;
        }

        @Override
        public T next() {
            T save = node.getValue();
            node = node.getNext();
            return save;
        }
    }
}
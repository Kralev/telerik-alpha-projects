package com.telerikacademy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTreeImpl implements BinarySearchTree {

    private BinaryTreeNode root;
    private static int height;

    public BinarySearchTreeImpl() {
        this.root = getRoot();
    }

    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    private BinaryTreeNode addRecursive(BinaryTreeNode current, int value) {
        if (current == null) {
            return new BinaryTreeNode(value);
        }

        if (value < current.getValue()) {
            current.setLeftChild(addRecursive(current.getLeftChild(), value));
        } else if (value > current.getValue()) {
            current.setRightChild(addRecursive(current.getRightChild(), value));
        } else {
            return current;
        }

        return current;
    }


    @Override
    public void insert(int value) {
        root = addRecursive(root, value);
    }

    @Override
    public BinaryTreeNode search(int value) {
        if (searchNodeRecursive(root, value) == null) {
            return null;
        } else {
            return searchNodeRecursive(root, value);
        }
    }

    private BinaryTreeNode searchNodeRecursive(BinaryTreeNode current, int value) {
        if (current == null) {
            return null;
        }
        if (value == current.getValue()) {
            return current;
        }
        return value < current.getValue() ? searchNodeRecursive(current.getLeftChild(), value) : searchNodeRecursive(current.getRightChild(), value);
    }

    public List<Integer> inOrder(BinaryTreeNode current, List<Integer> list) {
        if (current == null) {
            return new ArrayList<>(list);
        }
        inOrder(current.getLeftChild(), list);

        list.add(current.getValue());

        inOrder(current.getRightChild(), list);

        return new ArrayList<>(list);
    }

    @Override
    public List<Integer> inOrder() {
        return inOrder(root, new ArrayList<>());
    }

    public List<Integer> postOrder() {
        return postOrder(root, new ArrayList<>());
    }


    public List<Integer> postOrder(BinaryTreeNode node, List<Integer> list) {
        if (node == null)
            return new ArrayList<>(list);

        postOrder(node.getLeftChild(), list);

        postOrder(node.getRightChild(), list);

        list.add(node.getValue());

        return new ArrayList<>(list);
    }


    @Override
    public List<Integer> preOrder() {
        return preOrder(root, new ArrayList<>());
    }

    public List<Integer> preOrder(BinaryTreeNode node, List<Integer> list) {
        if (node == null)
            return new ArrayList<>(list);

        list.add(node.getValue());

        preOrder(node.getLeftChild(), list);

        preOrder(node.getRightChild(), list);

        return new ArrayList<>(list);
    }

    @Override
    public List<Integer> bfs() {
        List<Integer> list = new ArrayList<>();
        if (root == null) {
            return new ArrayList<>(list);
        }

        Queue<BinaryTreeNode> nodes = new LinkedList<>();


        nodes.add(root);

        while (!nodes.isEmpty()) {

            BinaryTreeNode node = nodes.remove();

            list.add(node.getValue());

            if (node.getLeftChild() != null) {
                nodes.add(node.getLeftChild());
            }

            if (node.getRightChild() != null) {
                nodes.add(node.getRightChild());
            }
        }

        return new ArrayList<>(list);
    }

    @Override
    public int height() {
        return height(root);
    }

    public int height(BinaryTreeNode current) {
        if (root == null) {
            return -1;
        }

        if (root.getRightChild() == null && root.getLeftChild() == null) {
            return 0;
        }

        if (current == null) {
            return -1;
        }

        int left = height(current.getLeftChild());
        int right = height(current.getRightChild());

        if (left > right) {
            return left + 1;
        } else {
            return right + 1;
        }
    }
}

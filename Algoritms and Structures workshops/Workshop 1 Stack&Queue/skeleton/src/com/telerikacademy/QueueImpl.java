package com.telerikacademy;

import java.util.LinkedList;

public class QueueImpl<T> implements Queue<T> {

    private LinkedList<T> queue;

    public QueueImpl() {
        queue = new LinkedList<>();
    }

    @Override
    public void offer(T elem) {
        queue.addLast(elem);
    }

    @Override
    public T poll() {
        if (!isEmpty())
            return queue.removeFirst();
        else throw new IllegalArgumentException("queue is empty");
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public T peek() {
        if (!isEmpty())
            return queue.getFirst();
        else throw new IllegalArgumentException("queue is empty");
    }

    @Override
    public int size() {
        return queue.size();
    }
}

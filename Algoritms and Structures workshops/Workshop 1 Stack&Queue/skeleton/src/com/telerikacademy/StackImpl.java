package com.telerikacademy;

public class StackImpl<T> implements Stack<T> {

    private T array[];
    private int top;
    private int maxSize;

    public StackImpl() {
        this.maxSize = 5;
        this.array = (T[]) new Integer[5];
        this.top = -1;
    }

    private T[] resizeArray() {
        int newSize = maxSize * 2;
        T[] newArray = (T[]) new Integer[newSize];
        for(int i = 0; i < maxSize; i++) {
            newArray[i] = this.array[i];
        }
        return newArray;
    }

    @Override
    public void push(T element) {
        if(!this.isFull()) {
            ++top;
            array[top] = element;
        }
        else {
            this.array = resizeArray();
            array[++top] = element;
        }
    }

    @Override
    public T pop() {
        if(!this.isEmpty())
            return array[top--];
        else {
            throw new IllegalArgumentException("Stack is empty");
        }
    }

    @Override
    public T peek() {
        if(!this.isEmpty())
            return array[top];
        else {
            throw new IllegalArgumentException("Stack is empty");
        }
    }

    @Override
    public int size() {
        return top + 1;
    }

   @Override
    public boolean isEmpty() {
        return top == -1;
    }

    public boolean isFull() {
        return top == maxSize-1;
    }
}
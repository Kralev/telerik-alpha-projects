package com.telerikacademy;

public interface Stack<T> {
      void push(T elem);
      T pop(); // връща/премахва го
      T peek(); // връща/ не го премахва
      int size();
      boolean isEmpty();
}

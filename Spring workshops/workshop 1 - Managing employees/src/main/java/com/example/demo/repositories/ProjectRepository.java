package com.example.demo.repositories;

import com.example.demo.models.Project;

import java.util.List;

public interface ProjectRepository {

    List<Project> getAll();
    void addNewProject(Project project);
    void removeProject(Project project);
    boolean exists(Project project);
    void changeProject(int index, Project project);
}

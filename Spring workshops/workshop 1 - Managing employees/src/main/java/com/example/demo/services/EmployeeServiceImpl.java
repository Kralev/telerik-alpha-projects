package com.example.demo.services;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.ProjectDto;
import com.example.demo.models.Employee;
import com.example.demo.models.Project;
import com.example.demo.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository repository;
    private ProjectService projectService;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository, ProjectService projectService) {
        this.repository = repository;
        this.projectService = projectService;
    }

    @Override
    public List<EmployeeDto> getAll() {
        return convertToDto(this.repository.getAll());
    }

    @Override
    public Employee getById(int id) {
        return this.repository
                .getAll()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(() ->
                        new IllegalArgumentException(String.format("Employee with id %d does not exist.", id)));
    }

    @Override
    public void addNewEmployee(Employee employee){

        this.repository.addNewEmployee(employee);
    }

    @Override
    public void removeEmployee(int id) {

        if (!employeeExists(id)){
            throw new IllegalArgumentException();
        }

        Employee employeeToBeRemoved = getById(id);

        this.repository.removeEmployee(employeeToBeRemoved);
    }

    @Override
    public void changeEmployee(int id, Employee employee) {
        if (!employeeExists(id)) {
            throw new IllegalArgumentException();
        }

        Employee employee1 = getById(id);

        int index = this.repository.getAll().indexOf(employee1);
        this.repository.changeEmployee(index, employee);
    }

    @Override
    public List<Employee> sortByNames() {
        return this.repository.getAll()
                .stream()
                .sorted((o1, o2) -> Comparator
                .comparing(Employee::getFirstName)
                .thenComparing(Employee::getLastName)
                .compare(o1,o2))
                .collect(Collectors.toList());
    }

    @Override
    public List<Employee> filterByName(String name) {
        return this.repository.getAll()
                .stream()
                .filter(employee -> employee.getFirstName().equals(name))
                .collect(Collectors.toList());
    }

    public void assignProject(int id, int  projectId) {
        if (!employeeExists(id)) {
            throw new IllegalArgumentException();
        }

        Employee employee1 = getById(id);
        Project project = this.projectService.getById(projectId);

        employee1.getProjects().add(project);
        project.getEmployees().add(employee1);
    }

    @Override
    public void unassignProject(int id, int projectId) {

        if (!employeeExists(id)) {
            throw new IllegalArgumentException();
        }

        Employee employee1 = getById(id);
        Project project = projectService.getById(projectId);

        employee1.getProjects().remove(project);
        project.getEmployees().remove(employee1);
    }

    public List<EmployeeDto> convertToDto(List<Employee> employees) {
        return employees.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private EmployeeDto convertToDto(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());

        List<ProjectDto> projects = Collections.emptyList();
        if (!CollectionUtils.isEmpty(employee.getProjects())) {
            projects = employee.getProjects().stream()
                    .map(project -> {
                        ProjectDto projectDto = new ProjectDto();
                        projectDto.setId(project.getId());
                        projectDto.setName(project.getName());
                        return projectDto;
                    }).collect(Collectors.toList());
        }

        employeeDto.setProjects(projects);
        return employeeDto;
    }

    public boolean employeeExists(int id){
        return this.repository.exists(getById(id));
    }

}
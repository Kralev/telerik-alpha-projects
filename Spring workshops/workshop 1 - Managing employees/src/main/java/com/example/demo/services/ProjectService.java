package com.example.demo.services;

import com.example.demo.dto.ProjectDto;
import com.example.demo.models.Project;

import java.util.List;

public interface ProjectService {

    List<ProjectDto> getAll();
    Project getById(int id);
    void addNewProject(Project project);
    void removeProject(int id);
    void changeProject(int id, Project project);
}

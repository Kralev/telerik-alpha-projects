package com.example.demo.repositories;

import com.example.demo.models.Project;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    private List<Project> projects;

    public ProjectRepositoryImpl() {
        this.projects = new ArrayList<>();

        projects.add(new Project(1, "First"));
        projects.add(new Project(2, "Second"));
        projects.add(new Project(3, "Third"));
    }

    @Override
    public List<Project> getAll() {
        return projects;
    }

    @Override
    public void addNewProject(Project project) {
        projects.add(project);
    }

    @Override
    public void removeProject(Project project) {
        projects.remove(project);
    }

    @Override
    public void changeProject(int index, Project project) {
        projects.set(index, project);
    }

    @Override
    public boolean exists(Project project) {
        return projects.contains(project);
    }
}

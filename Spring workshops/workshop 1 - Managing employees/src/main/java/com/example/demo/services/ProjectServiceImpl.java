package com.example.demo.services;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.ProjectDto;
import com.example.demo.models.Project;
import com.example.demo.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository repository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ProjectDto> getAll() {
        return convertToDto(repository.getAll());
    }

    @Override
    public Project getById(int id) {
        return repository
                .getAll()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(
                        () -> new IllegalArgumentException(String.format("Project with id %d does not exist.", id)));
    }

    @Override
    public void addNewProject(Project project) {
        repository.addNewProject(project);
    }

    @Override
    public void removeProject(int id) {
        if (!projectExists(id)){
            throw new IllegalArgumentException();
        }

        Project projectToBeRemoved = getById(id);

        repository.removeProject(projectToBeRemoved);
    }

    @Override
    public void changeProject(int id, Project project) {
        if (!projectExists(id)) {
            throw new IllegalArgumentException();
        }

        Project projectForChange = getById(id);

        int index = repository.getAll().indexOf(projectForChange);
        repository.changeProject(index, project);
    }

    private List<ProjectDto> convertToDto(List<Project> projects) {
        return projects.stream()
                .map(this::converToDto)
                .collect(Collectors.toList());
    }

    private ProjectDto converToDto(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());

        List<EmployeeDto> employees = Collections.emptyList();
        if (!CollectionUtils.isEmpty(project.getEmployees())) {
            employees = project.getEmployees().stream()
                    .map(employee -> {
                        EmployeeDto employeeDto = new EmployeeDto();
                        employeeDto.setId(employee.getId());
                        employeeDto.setFirstName(employee.getFirstName());
                        employeeDto.setLastName(employee.getLastName());
                        return employeeDto;
                    }).collect(Collectors.toList());
        }

        projectDto.setEmployees(employees);
        return projectDto;
    }

    private boolean projectExists(int id){
        return repository.exists(getById(id));
    }
}
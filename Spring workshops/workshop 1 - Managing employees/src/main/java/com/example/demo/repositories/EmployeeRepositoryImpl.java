package com.example.demo.repositories;

import com.example.demo.models.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository{

    private List<Employee> employees;

    public EmployeeRepositoryImpl() {
        this.employees = new ArrayList<>();

        employees.add(new Employee(1, "Petar", "Petrov"));
        employees.add(new Employee(2, "Ivan", "Ivanov"));
        employees.add(new Employee(3, "Petar", "Ivanov"));
    }

    @Override
    public List<Employee> getAll() {
        return this.employees;
    }

    @Override
    public void addNewEmployee(Employee employee) {
        this.employees.add(employee);
    }

    @Override
    public void removeEmployee(Employee employee) {
        this.employees.remove(employee);
    }

    @Override
    public void changeEmployee(int index, Employee employee) {
        this.employees.set(index, employee);
    }

    @Override
    public boolean exists(Employee employee) {
        return this.employees.contains(employee);
    }
}

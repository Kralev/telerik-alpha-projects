package com.example.demo.services;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.models.Employee;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDto> getAll();
    Employee getById(int id);
    void addNewEmployee(Employee employee);
    void removeEmployee(int id);
    void changeEmployee(int id, Employee employee);
    List<Employee> sortByNames();
    List<Employee> filterByName(String name);
    void assignProject(int id, int projectId);
    void unassignProject(int id, int projectId);
}

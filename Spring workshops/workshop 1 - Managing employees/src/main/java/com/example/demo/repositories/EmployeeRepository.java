package com.example.demo.repositories;

import com.example.demo.models.Employee;

import java.util.List;

public interface EmployeeRepository {

    List<Employee> getAll();
    void addNewEmployee(Employee employee);
    void removeEmployee(Employee employee);
    boolean exists(Employee employee);
    void changeEmployee(int index, Employee employee);
}
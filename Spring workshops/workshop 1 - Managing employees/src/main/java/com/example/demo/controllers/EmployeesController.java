package com.example.demo.controllers;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.models.Employee;
import com.example.demo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeesController {

    private EmployeeService service;

    @Autowired
    public EmployeesController(EmployeeService service) {
        this.service = service;
    }

    @GetMapping
    public List<EmployeeDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Employee getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody Employee employee) {
        service.addNewEmployee(employee);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.removeEmployee(id);
    }

    @PutMapping("/{id}")
    public void put(@PathVariable int id, @RequestBody @Valid Employee employee) {
        service.changeEmployee(id, employee);
    }

    @PutMapping("/assign/{id}/{projectId}")
    public void addProjectToEmployee(@PathVariable int id, @PathVariable int projectId) {
        service.assignProject(id, projectId);
    }

    @PutMapping("/unassign/{id}/{projectId}")
    public void removeProjectToEmployee(@PathVariable int id, @PathVariable int projectId) {
        service.unassignProject(id, projectId);
    }
}
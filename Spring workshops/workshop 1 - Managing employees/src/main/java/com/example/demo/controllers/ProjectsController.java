package com.example.demo.controllers;

import com.example.demo.dto.ProjectDto;
import com.example.demo.models.Project;
import com.example.demo.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsController {

    private ProjectService service;

    @Autowired
    public ProjectsController(ProjectService service) {
        this.service = service;
    }

    @GetMapping
    public List<ProjectDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Project getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody Project project) {
        service.addNewProject(project);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        service.removeProject(id);
    }

    @PutMapping("/{id}")
    public void put(@Valid @PathVariable int id, @RequestBody Project project) {
        service.changeProject(id, project);
    }
}
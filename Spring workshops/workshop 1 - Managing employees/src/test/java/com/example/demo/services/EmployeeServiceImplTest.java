package com.example.demo.services;

import com.example.demo.models.Employee;
import com.example.demo.repositories.EmployeeRepository;
import com.example.demo.repositories.EmployeeRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest {

    private List<Employee> employees;

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Before
    public void setUp() {
        employees = new ArrayList<>();
        employees.add(new Employee(1, "FirstName1", "LastName1"));
        employees.add(new Employee(2, "FirstName2", "LastName2"));
        employees.add(new Employee(3, "FirstName3", "LastName3"));
    }

    @Test
    public void getById_Should_ReturnRightEmployee_WhenExist() {
        Mockito.when(employeeRepository.getAll()).thenReturn(employees);

        Employee employeeResult = employeeService.getById(3);

        assertEquals(3, employeeResult.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_Throw_ExceptionForInvalidEmployee() {
        Mockito.when(employeeRepository.getAll()).thenReturn(employees);

        Employee employeeResult = employeeService.getById(5);
    }

    @Test
    public void getAll_Should_RetrieveAllEmployees() {
        Mockito.when(employeeRepository.getAll()).thenReturn(employees);

        assertEquals(3, employeeService.getAll().size());
    }


    @Test
    public void addNewEmployee_Should_AddNewEmployee() {
        Mockito.when(employeeRepository.getAll()).thenReturn(employees);
        //Mockito.when(employeeService.getAll()).thenReturn((e.getAll()));

        Employee employee = new Employee(4, "Simon", "Simpson");
        employeeService.addNewEmployee(employee);


        Mockito.verify(employeeRepository, Mockito.times(1)).addNewEmployee(employee);

    }

    @Test
    public void removeEmployee_Should_RemoveEmployee() {
        Mockito.when(employeeRepository.getAll()).thenReturn(employees);
        Mockito.when(employeeService.getById(1)).thenReturn(employees.get(0));
        Mockito.when(employeeService.employeeExists(1)).thenReturn(true);
        Mockito.when(employeeRepository.exists(employees.get(0))).thenReturn(true);


        employeeService.removeEmployee(1);

        Mockito.verify(employeeRepository, Mockito.times(1)).removeEmployee(employees.get(0));

    }
}